package ch.elca.fitch.producer;

import static ch.elca.fitch.producer.KafkaProducerConfig.NOTATION_CHANGED_TOPIC;

import java.math.BigDecimal;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaProducer {

  private static final Random RANDOM = new Random();

  private KafkaTemplate<String, String> kafkaTemplate;

  public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  @Scheduled(fixedRate = 5000)
  public void produceQuotation() {
    TickerSymbol symbol = getRandomTickerSymbol();
    Notation notation = getRandomNotation();
    kafkaTemplate.send(NOTATION_CHANGED_TOPIC, symbol.name(), notation.toString());
    log.info("Fitch changed notation {}: {}", symbol, notation);
  }

  private TickerSymbol getRandomTickerSymbol() {
    return TickerSymbol.values()[RANDOM.nextInt(TickerSymbol.values().length)];
  }

  private Notation getRandomNotation() {
    return Notation.values()[RANDOM.nextInt(TickerSymbol.values().length)];
  }

}
