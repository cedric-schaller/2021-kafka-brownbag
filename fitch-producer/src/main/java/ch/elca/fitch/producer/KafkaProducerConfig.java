package ch.elca.fitch.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaProducerConfig {

  public static final String NOTATION_CHANGED_TOPIC = "notationChanged";

  // not what you would typically do in a productive application
  @Bean
  public NewTopic topic() {
    return TopicBuilder.name(NOTATION_CHANGED_TOPIC)
      .partitions(10)
      .replicas(1)
      .build();
  }
}
