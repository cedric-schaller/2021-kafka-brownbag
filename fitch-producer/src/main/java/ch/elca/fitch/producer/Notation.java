package ch.elca.fitch.producer;

public enum Notation {
  AAA,
  AA,
  BBB,
  BB,
  CCC
}
