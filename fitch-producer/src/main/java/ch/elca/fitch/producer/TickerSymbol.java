package ch.elca.fitch.producer;

public enum TickerSymbol {
  AAPL,
  FB,
  MSFT,
  TSLA
}
