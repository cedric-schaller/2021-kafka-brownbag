package ch.elca.internalvaluation.producer;

import java.math.BigDecimal;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaProducer {

  private static final Random RANDOM = new Random();

  private KafkaTemplate<String, String> kafkaTemplate;

  public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }

  @Scheduled(fixedRate = 3000)
  public void produceQuotation() {
    TickerSymbol symbol = getRandomTickerSymbol();
    BigDecimal price = getRandomPrice();
    kafkaTemplate.send(KafkaProducerConfig.INTERNAL_VALUATION_CHANGED_TOPIC, symbol.name(), price.toString());
    log.info("Internal valuation {}: ${}", symbol, price);
  }

  private TickerSymbol getRandomTickerSymbol() {
    return TickerSymbol.values()[RANDOM.nextInt(TickerSymbol.values().length)];
  }

  private BigDecimal getRandomPrice() {
    return BigDecimal.valueOf(RANDOM.nextInt(20000), 2);
  }

}
