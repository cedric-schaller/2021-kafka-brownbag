package ch.elca.internalvaluation.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaProducerConfig {

  public static final String INTERNAL_VALUATION_CHANGED_TOPIC = "internalValuationChanged";

  // not what you would typically do in a productive application
  @Bean
  public NewTopic topic() {
    return TopicBuilder.name(INTERNAL_VALUATION_CHANGED_TOPIC)
      .partitions(10)
      .compact()
      .replicas(1)
      .build();
  }
}
