package ch.elca.internalvaluation.producer;

public enum TickerSymbol {
  AAPL,
  FB,
  MSFT,
  TSLA
}
