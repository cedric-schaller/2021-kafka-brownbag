package ch.elca.kafkastreamsconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaStreamsConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaStreamsConsumerApplication.class, args);
	}

}
