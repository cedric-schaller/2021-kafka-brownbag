package ch.elca.kafkastreamsconsumer;

import static ch.elca.kafkastreamsconsumer.Action.BUY;
import static ch.elca.kafkastreamsconsumer.Action.SELL;
import static ch.elca.kafkastreamsconsumer.Notation.AA;
import static ch.elca.kafkastreamsconsumer.Notation.AAA;

import java.util.function.Consumer;
import java.util.function.Function;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class KafkaStreamsConsumerConfig {

  @Bean
  public Function<KTable<String, String>,
    Function<KTable<String, String>,
      Consumer<KTable<String, String>>>> processEvents() {

    return stockQuotationChanged ->
      internalValuationChanged ->
        notationChanged ->
          stockQuotationChanged
            .mapValues(
              (tickerSymbol, quotation) -> Stock.forTickerSymbol(tickerSymbol).quotation(quotation))
            .join(internalValuationChanged,
              (stock, internalValuation) -> stock.internalValuation(internalValuation))
            .join(notationChanged, (stock, notation) -> stock.notation(notation))
            .toStream()
            .mapValues(this::toAction)
            .foreach((k, v) -> log.info("{}: {}", k, v));
  }

  private Action toAction(Stock stock) {
    if (stock.getNotation() != AAA && stock.getNotation() != AA) {
      return SELL;
    } else if (stock.getQuotation().compareTo(stock.getInternalValuation()) < 0) {
      return SELL;
    }
    return BUY;
  }
}
