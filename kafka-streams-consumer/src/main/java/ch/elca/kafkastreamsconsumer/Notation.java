package ch.elca.kafkastreamsconsumer;

public enum Notation {
  AAA,
  AA,
  BBB,
  BB,
  CCC
}
