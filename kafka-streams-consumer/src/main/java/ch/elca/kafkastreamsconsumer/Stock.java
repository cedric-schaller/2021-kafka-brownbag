package ch.elca.kafkastreamsconsumer;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Value;

@Value
public class Stock {
  TickerSymbol tickerSymbol;
  BigDecimal quotation;
  BigDecimal internalValuation;
  Notation notation;

  public static Stock forTickerSymbol(String tickerSymbolString) {
    TickerSymbol tickerSymbol = TickerSymbol.valueOf(tickerSymbolString);
    return new Stock(tickerSymbol, null, null, null);
  }

  public Stock quotation(String quotationString) {
    BigDecimal quotation = new BigDecimal(quotationString);
    return new Stock(this.tickerSymbol, quotation, this.internalValuation, this.notation);
  }

  public Stock internalValuation(String internalValuationString) {
    BigDecimal internalValuation = new BigDecimal(internalValuationString);
    return new Stock(this.tickerSymbol, this.quotation, internalValuation, this.notation);
  }

  public Stock notation(String notationString) {
    Notation notation = Notation.valueOf(notationString);
    return new Stock(this.tickerSymbol, this.quotation, this.internalValuation, notation);
  }
}
