package ch.elca.kafkastreamsconsumer;

public enum TickerSymbol {
  AAPL,
  FB,
  MSFT,
  TSLA
}
