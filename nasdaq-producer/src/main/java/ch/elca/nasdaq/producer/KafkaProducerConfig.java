package ch.elca.nasdaq.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaProducerConfig {

  public static final String STOCK_QUOTATION_CHANGED_TOPIC = "stockQuotationChanged";

  // not what you would typically do in a productive application
  @Bean
  public NewTopic topic() {
    return TopicBuilder.name(STOCK_QUOTATION_CHANGED_TOPIC)
      .partitions(10)
      .replicas(1)
      .build();
  }
}
