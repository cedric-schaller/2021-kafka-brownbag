package ch.elca.nasdaq.producer;

public enum TickerSymbol {
  AAPL,
  FB,
  MSFT,
  TSLA
}
