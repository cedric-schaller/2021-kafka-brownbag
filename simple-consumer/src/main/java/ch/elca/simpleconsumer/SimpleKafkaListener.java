package ch.elca.simpleconsumer;

import static ch.elca.simpleconsumer.Topics.INTERNAL_VALUATION_CHANGED_TOPIC;
import static ch.elca.simpleconsumer.Topics.NOTATION_CHANGED_TOPIC;
import static ch.elca.simpleconsumer.Topics.STOCK_QUOTATION_CHANGED_TOPIC;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SimpleKafkaListener {

  @KafkaListener(topics = {STOCK_QUOTATION_CHANGED_TOPIC, NOTATION_CHANGED_TOPIC, INTERNAL_VALUATION_CHANGED_TOPIC}, groupId = "simpleConsumer")
  public void consumeTopics(ConsumerRecord<String, String> consumerRecord) {
    String msg = "";
    switch (consumerRecord.topic()) {
      case STOCK_QUOTATION_CHANGED_TOPIC:
        msg = "Quotation received {}: {}";
        break;
      case NOTATION_CHANGED_TOPIC:
        msg = "Notation received {}: {}";
        break;
      case INTERNAL_VALUATION_CHANGED_TOPIC:
        msg = "Internal valuation received {}: {}";
        break;
      default:
        break;
    }
    log.info(msg, consumerRecord.key(), consumerRecord.value());
  }

}
