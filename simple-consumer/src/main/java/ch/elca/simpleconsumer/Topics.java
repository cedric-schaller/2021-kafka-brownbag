package ch.elca.simpleconsumer;

public final class Topics {

  public static final String STOCK_QUOTATION_CHANGED_TOPIC = "stockQuotationChanged";
  public static final String NOTATION_CHANGED_TOPIC = "notationChanged";
  public static final String INTERNAL_VALUATION_CHANGED_TOPIC = "internalValuationChanged";

  private Topics() {

  }

}
